# encoding: utf-8
# Bibliotecas

import sys
from sys import argv #Se necesita importar argv para obtener los parámetros
from workflow import Workflow, ICON_INFO, ICON_SYNC 

# Variables
script, argCifra = argv
fCifraOri = float(argCifra)
iIVA = 21

# Métodos

def main(miWF):
	def quitarIVA(infCifra, infIVACal):
		"""Devuelve una cifra sin el IVA
		
		Keyword arguments:
		infCifra -- Número a quitar IVA
		"""
		fCalculo = infCifra - fIVACal
		return fCalculo

	def anyadirIVA(infCifra, infIVACal):
		"""Devuelve una cifra añadiendo IVA
		
		Keyword arguments:
		infCifra -- Número a añadir IVA
		"""
		fCalculo = infCifra + infIVACal
		return fCalculo

	def calcularIVA(infCifra):
		"""Método que devuelve el IVA de una cifra
		
		Keyword arguments:
		infCifra -- Cifra que obtendrá el IVA
		"""
		return round(float(infCifra * (iIVA / 100.0)), 2)

	def formatearResultado(infCifra):
		"""Método que formatea una cifra dejando dos
		decimales y añadiendo al final el simbolo del euro
		
		Keyword arguments:
		infCifra -- Cifra que formateará
		"""
		return str('%0.2f' % (infCifra)) + u' \u20AC'

	# Lógica
	fIVACal = calcularIVA(fCifraOri)
	fCifraCalCon = anyadirIVA(fCifraOri, fIVACal)
	fCifraCalSin = quitarIVA(fCifraOri, fIVACal)

	#Imprimir resultados
	miWF.add_item(formatearResultado(fCifraCalCon), u'Con IVA', icon=ICON_INFO)
        miWF.add_item(formatearResultado(fCifraCalSin), u'Sin IVA', icon=ICON_INFO)
	miWF.add_item(formatearResultado(fIVACal), u'Impuesto total', icon=ICON_SYNC)

	miWF.send_feedback()

if __name__ == u"__main__":
	miWF = Workflow()
	sys.exit(miWF.run(main))
